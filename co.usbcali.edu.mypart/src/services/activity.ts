import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs';

import { config } from "./config";

@Injectable()
export class ActivityService {

  constructor(private http: Http) { }

  get(){
      let url = config.MYPARTSERVICES+"Activities/index";
      return this.http.get(url).toPromise()
                        .then(actividades => actividades.json() );
  }
  getActivity(id){
    let url = config.MYPARTSERVICES+"Activities/index?id="+id;
      return this.http.get(url).toPromise()
                        .then(actividades => actividades.json() );
  }
  updateStatus(id){
    let url = config.MYPARTSERVICES+"Activities/status";
    let headers = new Headers({'Content-Type':'application/x-www-form-urlencoded'});
    let data = this.jsonToUrlParams({id:id});
    //console.log(data);
      return this.http.put(url,data,{headers:headers});
  }

  post(tarea){
    let url = config.MYPARTSERVICES+"Activities/index";
    let headers = new Headers({'Content-Type':'application/x-www-form-urlencoded'});
    let data = this.jsonToUrlParams(tarea);
    //let data = "data="+JSON.stringify(tarea);
    return this.http.post(url,data,{headers:headers});
  }

  test(){
  
  //var creds = "username=" + username + "&password=" + password;
  let data: any = {
    a: "jhon",
    b: "foo",
    c: 45
  };
  let url = config.MYPARTSERVICES+"Activities/index";

  var headers = new Headers();
  headers.append('Content-Type', 'application/x-www-form-urlencoded');
  console.log("about to send a post request");
  this.http.post(url,this.jsonToUrlParams(data), {
    headers: headers
    })
    //.map(res => res.json())
    .subscribe(
      data => console.log(data),
      err => console.log(err),
      () => console.log('Post Request Done')
    );
  }

  jsonToUrlParams(json){
    return Object.keys(json)
    .map( key => encodeURIComponent(key) + '=' + encodeURIComponent(json[key]) )
    .join("&");
  }

}
