<?php

class Index_controller extends BServiceController {

    function __construct() {
        parent::__construct();
    }

    public function getIndex() {
        Request::setHeader(202, "text/html");
        echo "Get method Index controller";
    }

    public function postIndex() {
        Request::setHeader(202, "text/html");
        echo "Post method Index controller";
    }

    public function getSaludo($nombre, $apellido) {
        if (!isset($nombre) || !isset($apellido)) {
            throw new Exception('Paremetros insuficientes.');
        }
        Request::setHeader(200, "text/plain");
        echo "Hey " . $nombre . " " . $apellido . "!";
    }
    
    public function getEstadoFisico($altura,$peso){
        $client = new CUrlClient("http://10.0.1.193/satifit/services/Index/imc?altura=180&peso=90/Index/IMC?altura=$altura&peso=$peso");
        $imc = $client->execute()["imc"];
        
        $r["estado"] = "";
        
        if($imc < 18.5){
            $r["estado"] = "Muy flaco!";
        }elseif($imc >= 18.5 && $imc <=24.9){
            $r["estado"] = "Normi";
        }elseif ($imc >= 25 && $imc <=29.9) {
            $r["estado"] = "Gordito";
        }else{
            $r["estado"] = "Otra victima de la ballena Azul";
        }
        print json_encode($r);
        
    }
}
