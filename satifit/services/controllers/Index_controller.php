<?php

class Index_controller extends BServiceController {

    function __construct() {
        parent::__construct();
    }
    
    public function getIMC($altura,$peso){
        $m2 = ($altura/100) * ($altura/100);
        $imc["imc"] = round($peso /$m2 , 1);
        print json_encode( $imc );
    }
}
